package de.sabbertran.proxysuite.bukkit;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import de.sabbertran.proxysuite.bukkit.commands.BunCommand;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class ProxySuiteBukkit extends JavaPlugin {

    private HashMap<String, Location> pendingLocationTeleports;
    private HashMap<String, String> pendingPlayerTeleports;
    private HashMap<String, World> pendingSpawnTeleports;

    private WorldEditPlugin worldEdit;
    private boolean worldguardLoaded;
    private Chat chat;

    @Override
    public void onEnable() {
        pendingLocationTeleports = new HashMap<>();
        pendingPlayerTeleports = new HashMap<>();
        pendingSpawnTeleports = new HashMap<>();

        getConfig().addDefault("ProxySuite.HideLoginMessage", true);
        getConfig().addDefault("ProxySuite.HideLogoutMessage", true);
        getConfig().options().copyDefaults(true);
        saveConfig();

        getServer().getMessenger().registerOutgoingPluginChannel(this, "sabbertran:proxysuite");
        getServer().getMessenger().registerIncomingPluginChannel(this, "sabbertran:proxysuite", new PMessageListener(this));

        if (getServer().getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(Chat.class);
            if (chatProvider != null)
                chat = chatProvider.getProvider();
        }

        worldguardLoaded = getServer().getPluginManager().getPlugin("WorldGuard") != null;

        getServer().getPluginManager().registerEvents(new Events(this), this);

        getCommand("bun").setExecutor(new BunCommand(this));

        getLogger().info(getDescription().getName() + " " + getDescription().getVersion() + " by " + getDescription().getAuthors().get(0) + " enabled");
    }

    @Override
    public void onDisable() {
        getLogger().info(getDescription().getName() + " " + getDescription().getVersion() + " by " + getDescription().getAuthors().get(0) + " disabled");
    }

    public Chat getChat() {
        return chat;
    }

    public HashMap<String, Location> getPendingLocationTeleports() {
        return pendingLocationTeleports;
    }

    public HashMap<String, String> getPendingPlayerTeleports() {
        return pendingPlayerTeleports;
    }

    public HashMap<String, World> getPendingSpawnTeleports() {
        return pendingSpawnTeleports;
    }

    public WorldEditPlugin getWorldEdit() {
        return worldEdit;
    }

    public boolean isWorldguardLoaded() {
        return worldguardLoaded;
    }
}
