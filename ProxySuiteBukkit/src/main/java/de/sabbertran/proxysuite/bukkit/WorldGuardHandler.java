package de.sabbertran.proxysuite.bukkit;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import org.bukkit.entity.Player;

public class WorldGuardHandler {
    private ProxySuiteBukkit main;

    public WorldGuardHandler(ProxySuiteBukkit main) {
        this.main = main;
    }

    public static boolean canExecuteCommand(Player p, String cmd) {
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();


        BlockVector3 vectorLocation = BlockVector3.at(p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ());

        ApplicableRegionSet set = container.get(BukkitAdapter.adapt(p.getWorld())).getApplicableRegions(vectorLocation);
        if (set.size() > 0) {
            ProtectedRegion reg = getHighestRegion(set);
            if (reg.getFlag(Flags.BLOCKED_CMDS) != null) {
                for (String s : reg.getFlag(Flags.BLOCKED_CMDS).toArray(new
                        String[0])) {
                    if (s.toLowerCase().equals("/" + cmd.toLowerCase()))
                        return false;
                }
            }
        }
        return true;
    }

    private static ProtectedRegion getHighestRegion(ApplicableRegionSet set) {
        ProtectedRegion highest = null;
        for (ProtectedRegion r : set)
            if (highest == null || r.getPriority() > highest.getPriority())
                highest = r;
        return highest;
    }
}
