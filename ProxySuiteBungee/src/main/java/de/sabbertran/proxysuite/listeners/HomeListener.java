package de.sabbertran.proxysuite.listeners;

import de.exceptionflug.protocolize.inventory.event.InventoryClickEvent;
import de.exceptionflug.protocolize.items.ItemStack;
import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.ScrollerInventory;
import de.sabbertran.proxysuite.utils.Home;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class HomeListener implements Listener {
    private ProxySuite main;

    public HomeListener(ProxySuite main) {
        this.main = main;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        final ItemStack clicked = event.getClickedItem();
        final BaseComponent[] baseComponents = event.getInventory().getTitle();
        final ProxiedPlayer p = event.getPlayer();
        if (clicked == null) return;
        if (!ScrollerInventory.users.containsKey(p.getUniqueId())) return;
        if (baseComponents.length == 0) return;

        ScrollerInventory inv = ScrollerInventory.users.get(p.getUniqueId());

        if (!main.getHomeHandler().isHomeInventory(inv)) return;

        String previousPageName = main.getMessageHandler().getMessage("gui.previousPage", true);
        String nextPageName = main.getMessageHandler().getMessage("gui.nextPage", true);

        if (!clicked.getDisplayName().equals(nextPageName) && !clicked.getDisplayName().equals(previousPageName)) {
            String inventoryOwner = main.getHomeHandler().getHomeGuiOwners().get(inv);
            String homeNameEntryMessage = main.getMessageHandler().getMessage("home.list.entry.name");
            String[] homeMessageSplitted = homeNameEntryMessage.split("%home%", 3);

            String prefixFromPlaceholder = homeMessageSplitted[0];
            String suffixFromPlaceholder = homeMessageSplitted[1];

            String homeName = ChatColor.stripColor(clicked.getDisplayName().replaceAll(prefixFromPlaceholder, "").replaceAll(suffixFromPlaceholder, ""));

            if (main.getPermissionHandler().hasPermission(p, "proxysuite.commands.home")) {
                Home h = main.getHomeHandler().getHome(inventoryOwner, homeName);
                if (h != null) {
                    int remainingCooldown = main.getTeleportHandler().getRemainingCooldown(p);
                    boolean ignoreCooldown = main.getTeleportHandler().canIgnoreCooldown(p);
                    if (remainingCooldown == 0 || ignoreCooldown) {
                        main.getTeleportHandler().teleportToHome(p, h, ignoreCooldown);
                    } else {
                        main.getMessageHandler().sendMessage(p, main.getMessageHandler().getMessage
                                ("teleport.cooldown").replace("%cooldown%", "" + remainingCooldown));
                    }
                } else {
                    main.getMessageHandler().sendMessage(p, main.getMessageHandler().getMessage
                            ("home.notset").replace("%home%", homeName));
                }
            } else {
                main.getPermissionHandler().sendMissingPermissionInfo(p);
            }
        }
    }
}
