package de.sabbertran.proxysuite.listeners;

import de.exceptionflug.protocolize.inventory.event.InventoryClickEvent;
import de.exceptionflug.protocolize.items.ItemStack;
import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.ScrollerInventory;
import de.sabbertran.proxysuite.utils.Warp;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class WarpListener implements Listener {
    private ProxySuite main;

    public WarpListener(ProxySuite main) {
        this.main = main;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        final ItemStack clicked = event.getClickedItem();
        final BaseComponent[] baseComponents = event.getInventory().getTitle();
        final ProxiedPlayer p = event.getPlayer();
        if (clicked == null) return;
        if (!ScrollerInventory.users.containsKey(p.getUniqueId())) return;
        if (baseComponents.length == 0) return;

        ScrollerInventory inv = ScrollerInventory.users.get(p.getUniqueId());

        if (!main.getWarpHandler().isWarpInventory(inv)) return;

        String previousPageName = main.getMessageHandler().getMessage("gui.previousPage", true);
        String nextPageName = main.getMessageHandler().getMessage("gui.nextPage", true);

        if (!clicked.getDisplayName().equals(nextPageName) && !clicked.getDisplayName().equals(previousPageName)) {
            String warpNameEntryMessage = main.getMessageHandler().getMessage("warp.list.entry.name");
            String[] warpMessageSplitted = warpNameEntryMessage.split("%warp%", 3);

            String prefixFromPlaceholder = warpMessageSplitted[0];
            String suffixFromPlaceholder = warpMessageSplitted[1];

            String warpName = ChatColor.stripColor(clicked.getDisplayName().replaceAll(prefixFromPlaceholder, "").replaceAll(suffixFromPlaceholder, ""));

            boolean includeHidden = main.getPermissionHandler().hasPermission(p, "proxysuite.warps.showhidden");
            Warp w = main.getWarpHandler().getWarp(warpName, includeHidden);
            if (w != null) {
                int remainingCooldown = main.getTeleportHandler().getRemainingCooldown(p);
                boolean ignoreCooldown = main.getTeleportHandler().canIgnoreCooldown(p);
                if (remainingCooldown == 0 || ignoreCooldown) {
                    main.getTeleportHandler().teleportToWarp(p, w, ignoreCooldown);
                } else {
                    main.getMessageHandler().sendMessage(p, main.getMessageHandler().getMessage
                            ("teleport.cooldown").replace("%cooldown%", "" + remainingCooldown));
                }
            } else {
                main.getMessageHandler().sendMessage(p, main.getMessageHandler().getMessage
                        ("warp.notexists").replace("%warp%", warpName));
            }
        }

    }
}
