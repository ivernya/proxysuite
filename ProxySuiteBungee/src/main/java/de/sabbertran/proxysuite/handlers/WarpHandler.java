package de.sabbertran.proxysuite.handlers;

import de.exceptionflug.protocolize.items.ItemStack;
import de.exceptionflug.protocolize.items.ItemType;
import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.ScrollerInventory;
import de.sabbertran.proxysuite.utils.Location;
import de.sabbertran.proxysuite.utils.Warp;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WarpHandler implements Listener {

    private ProxySuite main;
    private ArrayList<Warp> warps;
    private List<ScrollerInventory> warpGuis;

    public WarpHandler(ProxySuite main) {
        this.main = main;
        warps = new ArrayList<>();
        warpGuis = new ArrayList<>();
    }

    public void readWarpsFromDatabase() {
        warps.clear();
        try {
            ResultSet rs = main.getSQLConnection().createStatement().executeQuery("SELECT * FROM " + main
                    .getTablePrefix() + "warps");
            while (rs.next()) {
                Warp w = new Warp(rs.getString("name"), new Location(main.getProxy()
                        .getServerInfo(rs
                                .getString("server")), rs.getString("world"), rs.getDouble("x"), rs.getDouble("y"), rs
                        .getDouble("z"), rs.getFloat("pitch"), rs.getFloat("yaw")), rs.getBoolean("hidden"));
                warps.add(w);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setWarp(String name, Location loc, boolean hidden) {
        String sql;
        Warp old = getWarp(name, true);
        if (old != null) {
            warps.remove(old);
            sql = "UPDATE " + main.getTablePrefix() + "warps SET `name` = '" + name + "', `hidden` = '" + (hidden ? 1
                    : 0) + "', `server` = '" + loc.getServer().getName() + "', " + "`world` = '" + loc.getWorld() +
                    "'," + " `x` = '" + loc.getX() + "', `y` = '" + loc.getY() + "', `z` = '" + loc.getZ() + "', " +
                    "`pitch` = '" + loc.getPitch() + "', `yaw` = '" + loc.getYaw() + "' WHERE LOWER(name) = '" + name
                    .toLowerCase() + "'";
        } else {
            sql = "INSERT INTO " + main.getTablePrefix() + "warps (name, hidden, server, world, x, y, z, pitch, yaw) " +
                    "VALUES ('" + name + "', '" + (hidden ? 1 : 0) + "', '" + loc.getServer().getName() + "', '" +
                    loc.getWorld() + "', '" + loc.getX() + "', " + "'" + loc.getY() + "', '" + loc.getZ() + "', '" +
                    loc.getPitch() + "', '" + loc.getYaw() + "')";
        }
        Warp w = new Warp(name, loc, hidden);
        warps.add(w);

        final String sql2 = sql;
        main.getProxy().getScheduler().runAsync(main, () -> {
            try {
                main.getSQLConnection().createStatement().execute(sql2);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void deleteWarp(Warp w) {
        final String sql = "DELETE FROM " + main.getTablePrefix() + "warps WHERE `name` = '" + w.getName() + "'";
        warps.remove(w);
        main.getProxy().getScheduler().runAsync(main, () -> {
            try {
                main.getSQLConnection().createStatement().execute(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendWarpList(ProxiedPlayer player, boolean includeHidden) {
        ArrayList<Warp> warpArrayList = this.warps;
        ArrayList<ItemStack> warpsItems = new ArrayList<>();

        if (warpArrayList.size() < 1) {
            main.getMessageHandler().sendMessage(player, main.getMessageHandler().getMessage("warp.list.nofound"));
            return;
        }

        for (int i = 0; i < warpArrayList.size(); i++) {
            Warp w = warpArrayList.get(i);

            if (!w.isHidden() || includeHidden) {
                ItemStack warpStack = new ItemStack(ItemType.ENDER_PEARL);

                warpStack.setDisplayName(main.getMessageHandler().getMessage("warp.list.entry.name", true).replaceAll("%warp%", w.getName()));

                String replacedLore = main.getMessageHandler().getMessage("warp.list.entry.lores", true).replaceAll("%server%", w.getLocation().getServer()
                        .getName()).replaceAll("%world%", w.getLocation().getWorld()).replaceAll("%coordX%", "" + w
                        .getLocation().getXInt()).replaceAll("%coordY%", "" + w.getLocation().getYInt()).replaceAll
                        ("%coordZ%", "" + w.getLocation().getZInt());

                List<String> splittedLore = Arrays.asList(replacedLore.split("\\\\n"));

                warpStack.setLore(splittedLore);

                warpsItems.add(warpStack);
            }
        }

        String previousPageName = main.getMessageHandler().getMessage("gui.previousPage", true);
        String nextPageName = main.getMessageHandler().getMessage("gui.nextPage", true);

        ScrollerInventory warpGui = new ScrollerInventory(warpsItems, main.getMessageHandler().getMessage("warp.list.guiName", true), previousPageName, nextPageName, player);
        warpGuis.add(warpGui);
    }

    public Warp getWarp(String name, boolean includeHidden) {
        if (name != null) {
            for (Warp w : warps) {
                if (w.getName().equalsIgnoreCase(name) && (!w.isHidden() || includeHidden)) {
                    return w;
                }
            }
        }
        return null;
    }

    public ArrayList<Warp> getWarps() {
        return warps;
    }

    public boolean isWarpInventory(ScrollerInventory inventory) {
        return warpGuis.contains(inventory);
    }
}
