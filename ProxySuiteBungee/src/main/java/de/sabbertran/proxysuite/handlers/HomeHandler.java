package de.sabbertran.proxysuite.handlers;

import de.exceptionflug.protocolize.items.ItemStack;
import de.exceptionflug.protocolize.items.ItemType;
import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.ScrollerInventory;
import de.sabbertran.proxysuite.utils.Home;
import de.sabbertran.proxysuite.utils.Location;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class HomeHandler {

    private ProxySuite main;
    private HashMap<ProxiedPlayer, ArrayList<Home>> homes;
    private HashMap<ScrollerInventory, String> homeGuiOwners;
    private List<ScrollerInventory> homeGuis;

    public HomeHandler(ProxySuite main) {
        this.main = main;
        homes = new HashMap<>();
        homeGuiOwners = new HashMap<>();
        homeGuis = new ArrayList<>();
    }

    /**
     * ACHTUNG!!! Datenbankzugriff wird nicht asynchron ausgeführt! Methode nur aus asynchronem Kontext aufrufen wenn
     * der player nicht online ist
     *
     * @param player .
     * @param name   .
     * @return .
     */
    public Home getHome(String player, String name) {
        ProxiedPlayer p = main.getPlayerHandler().getPlayer(player, null, false);
        if (p != null && homes.containsKey(p)) {
            for (Home h : homes.get(p)) {
                if (h.getName().equalsIgnoreCase(name)) {
                    return h;
                }
            }
        } else {
            try {
                ResultSet rs = main.getSQLConnection().createStatement().executeQuery("SELECT " + main.getTablePrefix
                        () + "homes.* FROM " + main.getTablePrefix() + "homes, " + main.getTablePrefix() + "players " +
                        "WHERE " + main.getTablePrefix() + "homes.player = " + main.getTablePrefix() + "players.uuid"
                        + " AND " + main.getTablePrefix() + "players.name = '" + player + "' AND " + main
                        .getTablePrefix() + "homes.name = '" + name + "'");
                if (rs.next()) {
                    Location loc = new Location(main.getProxy().getServerInfo(rs.getString("server")), rs.getString
                            ("world"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("pitch"), rs.getFloat("yaw"));
                    return new Home(p, name, loc);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void setHome(String player, String name, String serverName, String world, double x, double y, double z, float yaw, float pitch) {
        String sql = "INSERT INTO " + main.getTablePrefix() + "homes (`player`, `name`, `server`, `world`, `x`, `y`, `z`, `pitch`, `yaw`) VALUES('" + player+ "', '" + name + "', '" + serverName + "', '" + world + "', " +x + ", " + y  + ", " + z + ", " + yaw + ", " + pitch + ");";


        try {
            System.out.println("THE SQL REQUEST: " + sql);
            main.getSQLConnection().createStatement().execute(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setHome(String player, String name, Location loc) {
        String sql;
        ProxiedPlayer p = main.getPlayerHandler().getPlayer(player, null, false);
        Home old = p != null ? getHome(p.getName(), name) : null;

        String sqlDel = "";

        if (old != null) {
            old.setName(name);
            old.setLocation(loc);
            sql = "UPDATE " + main.getTablePrefix() + "homes SET `name` = '" + name + "', `server` = '" + loc
                    .getServer().getName() + "', " + "`world` = '" + loc.getWorld() + "'," + " `x` = '" + loc.getX()
                    + "', `y` = '" + loc.getY() + "', `z` = '" + loc.getZ() + "', `pitch` = '" + loc.getPitch() + "'," +
                    " `yaw` = '" + loc.getYaw() + "' WHERE player = '" + p.getUniqueId() + "' AND LOWER(name) = '" +
                    name.toLowerCase() + "'";
        } else {
            if (p != null) {
                Home h = new Home(p, name, loc);
                homes.get(p).add(h);
            }

            sqlDel = "DELETE FROM " + main.getTablePrefix() + "homes WHERE LOWER(name) = '" + name.toLowerCase() + "'" +
                    " AND player IN (SELECT uuid FROM " + main.getTablePrefix() + "players WHERE LOWER(name) = '" + (p != null ? p
                    .getName().toLowerCase() : player.toLowerCase()) + "')";

            sql = "INSERT INTO " + main.getTablePrefix() + "homes (player, name, server, world, x, y, z, pitch, yaw) " +
                    "SELECT " + main.getTablePrefix() + "players.uuid, '" + name + "', '" + loc.getServer().getName()
                    + "', '" + loc.getWorld() + "', '" + loc.getX() + "', '" + loc.getY() + "', '" + loc.getZ() + "'," +
                    " '" + loc.getPitch() + "', '" + loc.getYaw() + "' FROM " + main.getTablePrefix() + "players " +
                    "WHERE LOWER(" + main.getTablePrefix() + "players.name) = '" + player.toLowerCase() + "'";
        }

        final String sql2 = sql;
        final String sqlDel2 = sqlDel;
        main.getProxy().getScheduler().runAsync(main, () -> {
            try {
                if (!sqlDel2.equals(""))
                    main.getSQLConnection().createStatement().execute(sqlDel2);
                main.getSQLConnection().createStatement().execute(sql2);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void deleteHome(final String player, final String name) {
        ProxiedPlayer p = main.getPlayerHandler().getPlayer(player, null, false);
        if (p != null) {
            for (Home h : homes.get(p)) {
                if (h.getName().equalsIgnoreCase(name)) {
                    homes.get(p).remove(h);
                    break;
                }
            }
        }

        main.getProxy().getScheduler().runAsync(main, () -> {
            String sql = "DELETE FROM " + main.getTablePrefix() + "homes WHERE LOWER(name) = '" + name
                    .toLowerCase() + "' AND player IN (SELECT uuid FROM " + main.getTablePrefix() + "players " +
                    "WHERE name = '" + player + "')";
            try {
                main.getSQLConnection().createStatement().execute(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public int getHomesInWorld(ProxiedPlayer p, ServerInfo server, String world) {
        int ret = 0;
        for (Home h : homes.get(p))
            if (h.getLocation().getServer().equals(server) && h.getLocation().getWorld().equals(world))
                ret++;
        return ret;
    }

    public void updateHomesFromDatabase(final ProxiedPlayer p) {
        main.getProxy().getScheduler().runAsync(main, () -> {
            homes.remove(p);
            ArrayList<Home> insert = new ArrayList<>();
            try {
                ResultSet rs = main.getSQLConnection().createStatement().executeQuery("SELECT * FROM " + main
                        .getTablePrefix() + "homes WHERE player = '" + p.getUniqueId() + "'");
                while (rs.next()) {
                    String name = rs.getString("name");
                    Location loc = new Location(main.getProxy().getServerInfo(rs.getString("server")), rs
                            .getString("world"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs
                            .getFloat("pitch"), rs.getFloat("yaw"));
                    Home h = new Home(p, name, loc);
                    insert.add(h);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            homes.put(p, insert);
        });
    }

    public ArrayList<Home> getHomesFromDatabase(final ProxiedPlayer p) {
        final ArrayList<Home> homes = new ArrayList<>();
        homes.add(new Home(null, null, null));

        if (p != null && this.homes.containsKey(p)) {
            homes.clear();
            homes.addAll(this.homes.get(p));
            if (main.getConfig().getBoolean("ProxySuite.Homes.SortAlphabetically")) {
                Collections.sort(homes, Comparator.comparing(o -> o.getName().toLowerCase()));
            }
        } else {
            main.getProxy().getScheduler().runAsync(main, () -> {
                try {
                    homes.clear();
                    PreparedStatement pst = main.getSQLConnection().prepareStatement("SELECT h.name, h.server, h.world, h.x, h.y, h.z, h.pitch, h.yaw FROM " + main.getTablePrefix() + "homes h " +
                            "INNER JOIN " + main.getTablePrefix() + "players p ON h.player = p.uuid " +
                            "WHERE p.name = ? " +
                            (main.getConfig().getBoolean("ProxySuite.Homes.SortAlphabetically") ? "ORDER BY LOWER(h.name)" : ""));
                    pst.setString(1, p.getName());
                    ResultSet rs = pst.executeQuery();
                    while (rs.next()) {
                        String name = rs.getString("name");
                        Location loc = new Location(main.getProxy().getServerInfo(rs.getString("server")), rs.getString
                                ("world"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("pitch"), rs.getFloat("yaw"));
                        Home h = new Home(null, name, loc);
                        homes.add(h);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }

        return homes;
    }

    public void sendHomeList(final ProxiedPlayer sender, final String player) {
        ProxiedPlayer p = main.getPlayerHandler().getPlayer(player, sender, false);

        ArrayList<Home> homes = getHomesFromDatabase(p);
        ArrayList<ItemStack> homesItems = new ArrayList<>();

        final ScheduledTask[] task = new ScheduledTask[1];
        task[0] = main.getProxy().getScheduler().schedule(main, () -> {
            if (homes.size() != 1 || (homes.get(0).getPlayer() != null && homes.get(0).getName() != null &&
                    homes.get(0).getLocation() != null)) {
                main.getProxy().getScheduler().cancel(task[0]);

                if (homes.size() < 1) {
                    main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("home.list.nofound"));
                    return;
                }

                for (Home h : homes) {
                    ItemStack warpStack = new ItemStack(ItemType.ENDER_PEARL);

                    warpStack.setDisplayName(main.getMessageHandler().getMessage("home.list.entry.name", true).replaceAll("%home%", h.getName()));

                    String replacedLore = main.getMessageHandler().getMessage("home.list.entry.lores", true).replaceAll("%server%", h.getLocation().getServer()
                            .getName()).replaceAll("%world%", h.getLocation().getWorld()).replaceAll("%coordX%", "" + h
                            .getLocation().getXInt()).replaceAll("%coordY%", "" + h.getLocation().getYInt()).replaceAll
                            ("%coordZ%", "" + h.getLocation().getZInt());

                    List<String> splittedLore = Arrays.asList(replacedLore.split("\\\\n"));

                    warpStack.setLore(splittedLore);

                    homesItems.add(warpStack);
                }

                String previousPageName = main.getMessageHandler().getMessage("gui.previousPage", true);
                String nextPageName = main.getMessageHandler().getMessage("gui.nextPage", true);

                String guiName;
                if (sender.getName().equals(player)) {
                    guiName = main.getMessageHandler().getMessage("home.list.guiName", true);
                } else {
                    guiName = main.getMessageHandler().getMessage("home.list.guiName.other", true).replaceAll("%player%", player);
                }

                ScrollerInventory homeGui = new ScrollerInventory(homesItems, guiName, previousPageName, nextPageName, sender);
                homeGuiOwners.put(homeGui, player);
                homeGuis.add(homeGui);
            }
        }, 10, 500, TimeUnit.MILLISECONDS);
    }

    public int getMaximumHomes(String player) {
        int highest = main.getConfig().getInt("ProxySuite.Homes.DefaultMaximum");
        if (main.getPermissionHandler().getPermissions().containsKey(player)) {
            if (main.getPermissionHandler().hasPermission(player, "proxysuite.commands.sethome.multiple.*"))
                return -1;
            for (String s : main.getPermissionHandler().getPermissions().get(player))
                if (s.startsWith("proxysuite.commands.sethome.multiple.")) {
                    String amount = s.replace("proxysuite.commands.sethome.multiple.", "");
                    try {
                        int temp = Integer.parseInt(amount);
                        if (temp > highest)
                            highest = temp;
                    } catch (NumberFormatException ex) {
                    }
                }
        }
        return highest;
    }

    public int getMaximumHomesPerWorld(String player) {
        int highest = main.getConfig().getInt("ProxySuite.Homes.DefaultMaximumPerWorld");
        if (main.getPermissionHandler().getPermissions().containsKey(player)) {
            if (main.getPermissionHandler().hasPermission(player, "proxysuite.commands.sethome.world.multiple.*"))
                return -1;
            for (String s : main.getPermissionHandler().getPermissions().get(player))
                if (s.startsWith("proxysuite.commands.sethome.world.multiple.")) {
                    String amount = s.replace("proxysuite.commands.sethome.world.multiple.", "");
                    try {
                        int temp = Integer.parseInt(amount);
                        if (temp > highest)
                            highest = temp;
                    } catch (NumberFormatException ex) {
                    }
                }
        }
        return highest;
    }

    public int getHomeAmount(ProxiedPlayer p) {
        return homes.containsKey(p) ? homes.get(p).size() : 0;
    }

    public void removeHomesFromCache(ProxiedPlayer p) {
        homes.remove(p);
    }

    public HashMap<ScrollerInventory, String> getHomeGuiOwners() {
        return homeGuiOwners;
    }

    public boolean isHomeInventory(ScrollerInventory inventory) {
        return homeGuis.contains(inventory);
    }
}
