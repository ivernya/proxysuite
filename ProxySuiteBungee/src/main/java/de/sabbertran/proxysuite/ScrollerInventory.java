package de.sabbertran.proxysuite;

import de.exceptionflug.protocolize.inventory.Inventory;
import de.exceptionflug.protocolize.inventory.InventoryModule;
import de.exceptionflug.protocolize.inventory.InventoryType;
import de.exceptionflug.protocolize.items.ItemStack;
import de.exceptionflug.protocolize.items.ItemType;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ScrollerInventory {
    public static HashMap<UUID, ScrollerInventory> users = new HashMap<>();
    public ArrayList<Inventory> pages = new ArrayList<>();
    public UUID id;
    public int currPage = 0;

    public ScrollerInventory(ArrayList<ItemStack> items, String name, String previousPageName, String nextPageName, ProxiedPlayer p) {
        this.id = UUID.randomUUID();
        Inventory page = getBlankPage(name, previousPageName, nextPageName);
        for (int i = 0; i < items.size(); i++) {
            if (firstEmpty(page) == 10) {
                pages.add(page);
                page = getBlankPage(name, previousPageName, nextPageName);
            }
            page.setItem(firstEmpty(page), items.get(i));
        }
        pages.add(page);

        InventoryModule.sendInventory(p, pages.get(currPage));
        users.put(p.getUniqueId(), this);
    }

    private int firstEmpty(Inventory inv) {
        List<ItemStack> inventory = inv.getItemsIndexed(578);
        for (int i = 0; i < inventory.size(); i++) {
            if (inventory.get(i) == null) {
                return i;
            }
        }
        return -1;
    }

    private Inventory getBlankPage(String name, String previousPageName, String nextPageName) {
        Inventory page = new Inventory(InventoryType.GENERIC_9X2, TextComponent.fromLegacyText(name));

        ItemStack nextPage = new ItemStack(ItemType.GREEN_STAINED_GLASS_PANE);
        nextPage.setDisplayName(nextPageName);

        ItemStack prevPage = new ItemStack(ItemType.GREEN_STAINED_GLASS_PANE);
        prevPage.setDisplayName(previousPageName);

        page.setItem(17, nextPage);
        page.setItem(9, prevPage);
        return page;
    }
}
