package de.sabbertran.proxysuite;

import de.exceptionflug.protocolize.inventory.InventoryModule;
import de.exceptionflug.protocolize.inventory.event.InventoryClickEvent;
import de.exceptionflug.protocolize.items.ItemStack;
import de.exceptionflug.protocolize.world.Sound;
import de.exceptionflug.protocolize.world.SoundCategory;
import de.exceptionflug.protocolize.world.WorldModule;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ScrollerInventoryListener implements Listener {
    private ProxySuite main;

    public ScrollerInventoryListener(ProxySuite main) {
        this.main = main;
    }

    public static boolean soundExists(String soundName) {
        for (Sound sound : Sound.values()) {
            if (sound.name().equals(soundName)) {
                return true;
            }
        }

        return false;
    }

    public static boolean soundCategoryExists(String soundCategoryName) {
        for (SoundCategory soundCategory : SoundCategory.values()) {
            if (soundCategory.name().equals(soundCategoryName)) {
                return true;
            }
        }

        return false;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        final ItemStack clicked = event.getClickedItem();
        final ProxiedPlayer p = event.getPlayer();
        if (clicked == null) return;
        if (!ScrollerInventory.users.containsKey(p.getUniqueId())) return;

        ScrollerInventory inv = ScrollerInventory.users.get(p.getUniqueId());
        String previousPageName = main.getMessageHandler().getMessage("gui.previousPage", true);
        String nextPageName = main.getMessageHandler().getMessage("gui.nextPage", true);

        if (clicked.getDisplayName().equals(nextPageName) || clicked.getDisplayName().equals(previousPageName)) {
            boolean soundEnabled = main.getConfig().getBoolean("ProxySuite.GuiSounds.PagesSound.Enabled");

            if (!soundEnabled) return;

            String soundName = main.getConfig().getString("ProxySuite.GuiSounds.PagesSound.Sound");
            String soundCategoryName = main.getConfig().getString("ProxySuite.GuiSounds.PagesSound.SoundCategory");

            if (!soundExists(soundName)) {
                main.getLogger().info("Received request to play invalid sound to " + p.getName() + ": " + soundName);
            } else if (!soundCategoryExists(soundCategoryName)) {
                main.getLogger().info("Received request to play invalid sound from an invalid category to " + p.getName() + ": " + soundCategoryName);
            } else {
                Sound sound = Sound.valueOf(soundName);
                SoundCategory soundCategory = SoundCategory.valueOf(soundCategoryName);
                float soundVolume = main.getConfig().getFloat("ProxySuite.GuiSounds.PagesSound.Volume");
                float soundPitch = main.getConfig().getFloat("ProxySuite.GuiSounds.PagesSound.Pitch");

                WorldModule.playSound(p, sound, soundCategory, soundVolume, soundPitch);
            }
        }

        if (clicked.getDisplayName().equals(nextPageName)) {
            event.setCancelled(true);
            if (inv.currPage < inv.pages.size() - 1) {
                inv.currPage += 1;
                InventoryModule.sendInventory(p, inv.pages.get(inv.currPage));
            }
        } else if (clicked.getDisplayName().equals(previousPageName)) {
            event.setCancelled(true);
            if (inv.currPage > 0) {
                inv.currPage -= 1;
                InventoryModule.sendInventory(p, inv.pages.get(inv.currPage));
            }
        }
    }
}
