package de.sabbertran.proxysuite.commands.home;

import com.google.common.collect.ImmutableSet;
import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.utils.Home;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.HashSet;
import java.util.Set;

public class DelHomeCommand extends Command implements TabExecutor {

    private ProxySuite main;
    private DelHomeCommand self;

    public DelHomeCommand(ProxySuite main) {
        super("delhome");
        this.main = main;
        this.self = this;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 2) {
            return ImmutableSet.of();
        }

        Set<String> matches = new HashSet<>();

        if (args.length < 2) {
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer p = (ProxiedPlayer) sender;
                for (Home home : main.getHomeHandler().getHomesFromDatabase(p)) {
                    matches.add(home.getName());
                }
            }
        } else {
            String playerArg = args[0];

            ProxiedPlayer p = main.getPlayerHandler().getPlayer(playerArg, sender, false);

            if (p == null) return matches;

            for (Home home : main.getHomeHandler().getHomesFromDatabase(p)) {
                matches.add(home.getName());
            }
        }

        return matches;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        main.getCommandHandler().executeCommand(sender, "delhome", () -> {
            if (args.length == 0) {
                if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.delhome")) {
                    if (sender instanceof ProxiedPlayer) {
                        final ProxiedPlayer p = (ProxiedPlayer) sender;
                        main.getProxy().getScheduler().runAsync(main, () -> {
                            if (main.getHomeHandler().getHome(p.getName(), "home") != null) {
                                main.getHomeHandler().deleteHome(p.getName(), "home");
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                        ("home.delete.success.default"));
                            } else {
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                        ("home.notset.default"));
                            }
                        });
                    } else {
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("command.noplayer"));
                    }
                } else {
                    main.getPermissionHandler().sendMissingPermissionInfo(sender);
                }
            } else if (args.length == 1) {
                if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.delhome")) {
                    if (sender instanceof ProxiedPlayer) {
                        final ProxiedPlayer p = (ProxiedPlayer) sender;
                        main.getProxy().getScheduler().runAsync(main, () -> {
                            Home h = main.getHomeHandler().getHome(p.getName(), args[0]);
                            if (h != null) {
                                main.getHomeHandler().deleteHome(p.getName(), h.getName());
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                        ("home.delete.success").replace("%home%", h.getName()));
                            } else {
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                        ("home.notset").replace("%home%", args[0]));
                            }
                        });
                    } else {
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("command.noplayer"));
                    }
                } else {
                    main.getPermissionHandler().sendMissingPermissionInfo(sender);
                }
            } else if (args.length == 2) {
                if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.delhome.others")) {
                    main.getProxy().getScheduler().runAsync(main, () -> {
                        Home h = main.getHomeHandler().getHome(args[0], args[1]);
                        if (h != null) {
                            main.getHomeHandler().deleteHome(args[0], h.getName());
                            main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                    ("home.delete.others.success").replace("%player%", args[0]).replace("%home%", h
                                    .getName()));
                        } else {
                            main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                    ("home.notset.others").replace("%player%", args[0]).replace("%home%", h
                                    .getName()));
                        }
                    });
                } else {
                    main.getPermissionHandler().sendMissingPermissionInfo(sender);
                }
            } else {
                main.getCommandHandler().sendUsage(sender, self);
            }
        });
    }
}
