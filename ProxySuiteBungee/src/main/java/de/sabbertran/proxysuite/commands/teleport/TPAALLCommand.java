package de.sabbertran.proxysuite.commands.teleport;

import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.utils.PendingTeleport;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TPAALLCommand extends Command {
    private ProxySuite main;
    private TPAALLCommand self;

    public TPAALLCommand(ProxySuite main) {
        super("tpaall");
        this.main = main;
        this.self = this;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        main.getCommandHandler().executeCommand(sender, "tpaall", () -> {
            if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.tpaall")) {
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer p = (ProxiedPlayer) sender;

                    for (ProxiedPlayer teleporter : main.getProxy().getPlayers()) {
                        if (teleporter == sender) continue;

                        PendingTeleport teleport = new PendingTeleport(main.getTeleportHandler(), PendingTeleport
                                .TeleportType.TPAHERE, teleporter, p, main.getConfig().getInt("ProxySuite.Teleport" +
                                ".Timeout"));
                        main.getTeleportHandler().getPendingTeleports().add(teleport);
                        main.getMessageHandler().sendMessage(teleporter, main.getMessageHandler().getMessage
                                ("teleport.tpahere.request.received").replace("%player%", p.getName()).replace("%prefix%", main
                                .getPlayerHandler().getPrefix(p)).replace("%suffix%", main
                                .getPlayerHandler().getSuffix(p)));
                    }

                    main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("teleport" +
                            ".requestAll.sent"));
                } else {
                    main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("command.noplayer"));
                }
            } else {
                main.getPermissionHandler().sendMissingPermissionInfo(sender);
            }
        });
    }
}
