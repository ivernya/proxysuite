package de.sabbertran.proxysuite.commands.home;

import com.google.common.collect.ImmutableSet;
import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.utils.Home;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.HashSet;
import java.util.Set;

public class HomeCommand extends Command implements TabExecutor {
    private ProxySuite main;
    private HomeCommand self;

    public HomeCommand(ProxySuite main) {
        super("home");
        this.main = main;
        this.self = this;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 2) {
            return ImmutableSet.of();
        }

        Set<String> matches = new HashSet<>();

        if (args.length < 2) {
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer p = (ProxiedPlayer) sender;
                for (Home home : main.getHomeHandler().getHomesFromDatabase(p)) {
                    matches.add(home.getName());
                }
            }
        } else {
            String playerArg = args[0];

            ProxiedPlayer p = main.getPlayerHandler().getPlayer(playerArg, sender, false);

            if (p == null) return matches;

            for (Home home : main.getHomeHandler().getHomesFromDatabase(p)) {
                matches.add(home.getName());
            }
        }

        return matches;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        main.getCommandHandler().executeCommand(sender, "home", () -> {
            if (sender instanceof ProxiedPlayer) {
                final ProxiedPlayer p = (ProxiedPlayer) sender;
                if (args.length == 0) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.home")) {
                        Home h = main.getHomeHandler().getHome(p.getName(), "home");
                        if (h != null) {
                            int remainingCooldown = main.getTeleportHandler().getRemainingCooldown(p);
                            boolean ignoreCooldown = main.getTeleportHandler().canIgnoreCooldown(sender);
                            if (remainingCooldown == 0 || ignoreCooldown) {
                                main.getTeleportHandler().teleportToHome(p, h, ignoreCooldown);
                            } else {
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                        ("teleport.cooldown").replace("%cooldown%", "" + remainingCooldown));
                            }
                        } else {
                            main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                    ("home.notset.default"));
                        }
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else if (args.length == 1) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.home")) {
                        Home h = main.getHomeHandler().getHome(p.getName(), args[0]);
                        if (h != null) {
                            int remainingCooldown = main.getTeleportHandler().getRemainingCooldown(p);
                            boolean ignoreCooldown = main.getTeleportHandler().canIgnoreCooldown(sender);
                            if (remainingCooldown == 0 || ignoreCooldown) {
                                main.getTeleportHandler().teleportToHome(p, h, ignoreCooldown);
                            } else {
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                        ("teleport.cooldown").replace("%cooldown%", "" + remainingCooldown));
                            }
                        } else {
                            main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                    ("home.notset").replace("%home%", args[0]));
                        }
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else if (args.length == 2) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.home.others")) {
                        final String player = args[0];
                        final String home = args[1];
                        main.getProxy().getScheduler().runAsync(main, () -> {
                            Home h = main.getHomeHandler().getHome(player, home);
                            if (h != null) {
                                int remainingCooldown = main.getTeleportHandler().getRemainingCooldown(p);
                                boolean ignoreCooldown = main.getTeleportHandler().canIgnoreCooldown(sender);
                                if (remainingCooldown == 0 || ignoreCooldown) {
                                    main.getTeleportHandler().teleportToHome(p, h, ignoreCooldown);
                                } else {
                                    main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                            ("teleport.cooldown").replace("%cooldown%", "" + remainingCooldown));
                                }
                            } else
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler()
                                        .getMessage("home.notset.others").replace("%home%", home).replace
                                                ("%player%", player));
                        });
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else {
                    main.getCommandHandler().sendUsage(sender, self);
                }
            } else {
                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("command.noplayer"));
            }
        });
    }
}
