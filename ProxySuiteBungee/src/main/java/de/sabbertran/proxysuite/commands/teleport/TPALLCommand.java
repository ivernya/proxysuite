package de.sabbertran.proxysuite.commands.teleport;

import de.sabbertran.proxysuite.ProxySuite;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TPALLCommand extends Command {
    private ProxySuite main;
    private TPALLCommand self;

    public TPALLCommand(ProxySuite main) {
        super("tpall");
        this.main = main;
        this.self = this;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        main.getCommandHandler().executeCommand(sender, "tpall", () -> {
            if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.tpall")) {
                if (sender instanceof ProxiedPlayer) {
                    ProxiedPlayer p = (ProxiedPlayer) sender;

                    for (ProxiedPlayer teleport : main.getProxy().getPlayers()) {
                        if (teleport == sender) continue;

                        boolean ignoreCooldown = main.getTeleportHandler().canIgnoreCooldown(sender);

                        main.getTeleportHandler().teleportToPlayer(teleport, p, ignoreCooldown);
                    }
                } else {
                    main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("command.noplayer"));
                }
            } else {
                main.getPermissionHandler().sendMissingPermissionInfo(sender);
            }
        });
    }
}
