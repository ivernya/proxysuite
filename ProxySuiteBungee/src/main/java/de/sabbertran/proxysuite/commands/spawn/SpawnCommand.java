package de.sabbertran.proxysuite.commands.spawn;

import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.utils.Location;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SpawnCommand extends Command {

    private ProxySuite main;

    public SpawnCommand(ProxySuite main) {
        super("spawn");
        this.main = main;
    }


    @Override
    public void execute(final CommandSender sender, String[] args) {
        main.getCommandHandler().executeCommand(sender, "spawn", () -> {
            if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.spawn")) {

                Location normalSpawn = main.getSpawnHandler().getNormalSpawn();

                if (normalSpawn == null) {
                    main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("spawn.notset"));
                    return;
                }

                if (args.length == 1) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.spawn.others")) {
                        String playerName = args[0];
                        ProxiedPlayer pl = main.getPlayerHandler().getPlayer(playerName, sender, true);

                        main.getTeleportHandler().teleportToLocation(pl, normalSpawn, true, false);
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("spawn.teleport.other").replace("%player%", playerName));
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else {
                    if (!(sender instanceof ProxiedPlayer)) {
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("command.noplayer"));
                        return;
                    }
                    ProxiedPlayer p = (ProxiedPlayer) sender;

                    int remainingCooldown = main.getTeleportHandler().getRemainingCooldown(p);
                    boolean ignoreCooldown = main.getTeleportHandler().canIgnoreCooldown(sender);
                    if (remainingCooldown == 0 || ignoreCooldown) {
                        main.getTeleportHandler().teleportToLocation(p, normalSpawn, ignoreCooldown, false);
                    } else {
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage
                                ("teleport.cooldown").replace("%cooldown%", "" + remainingCooldown));
                    }
                }
            } else {
                main.getPermissionHandler().sendMissingPermissionInfo(sender);
            }
        });
    }
}
