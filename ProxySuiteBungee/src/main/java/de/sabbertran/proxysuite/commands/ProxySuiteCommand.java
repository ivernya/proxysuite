package de.sabbertran.proxysuite.commands;

import com.google.common.collect.ImmutableSet;
import de.sabbertran.proxysuite.ProxySuite;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class ProxySuiteCommand extends Command implements TabExecutor {
    private ProxySuiteCommand self;
    private ProxySuite main;

    public ProxySuiteCommand(ProxySuite main) {
        super("proxysuite", null, new String[]{"ps", "deeznutzforpresident"});
        this.main = main;
        this.self = this;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 1) {
            return ImmutableSet.of();
        }

        Set<String> matches = new HashSet<>();
        matches.add("reloadmsg");
        matches.add("reloadann");
        matches.add("reloadperms");
        matches.add("reloadcfg");

        return matches;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        main.getCommandHandler().executeCommand(sender, "proxysuite", () -> {
            if (args.length == 1) {
                if ((args[0].equalsIgnoreCase("reloadmessages") || args[0].equalsIgnoreCase("reloadmsg"))) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.reloadmessages")) {
                        main.getMessageHandler().readMessagesFromFile();
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("messages.reload.success"));
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else if ((args[0].equalsIgnoreCase("reloadannouncements") || args[0].equalsIgnoreCase("reloadann"))) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.reloadannouncements")) {
                        main.getAnnouncementHandler().readAnnouncementsFromFile();
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("announcements.reload.success"));
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else if ((args[0].equalsIgnoreCase("reloadcfg") || args[0].equalsIgnoreCase("reloadconfig"))) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.reloadconfig")) {
                        try {
                            Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new InputStreamReader(new FileInputStream(main.getConfigFile()), "UTF8"));
                            main.setConfig(config);

                            main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("config.reload.success"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else if (args[0].equalsIgnoreCase("reloadperms")) {
                    if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.reloadperms")) {
                        main.getPermissionHandler().resetPermissions();
                        main.getPermissionHandler().updatePermissions();
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("permissions.reload.success"));
                    } else {
                        main.getPermissionHandler().sendMissingPermissionInfo(sender);
                    }
                } else if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("reloadperms")) {
                        ProxiedPlayer p = main.getPlayerHandler().getPlayer(args[1], sender, true);
                        if (p != null) {
                            if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.reloadperms")) {
                                main.getPermissionHandler().resetPermissions(p);
                                main.getPermissionHandler().updatePermissions(p);
                                main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("permissions.reload.success.player").replace("%player%", p.getName()));
                            } else {
                                main.getPermissionHandler().sendMissingPermissionInfo(sender);
                            }
                        } else {
                            main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("command.player.notseen").replace("%player%", args[1]));
                        }
                    }
                } else {
                    main.getCommandHandler().sendUsage(sender, self);
                }
            } else {
                main.getMessageHandler().sendMessage(sender, "&6Proxy&8Suite &r" + main.getDescription().getVersion() + " by &b" + main.getDescription().getAuthor());
            }
        });
    }
}
