package de.sabbertran.proxysuite.commands.warp;

import com.google.common.collect.ImmutableSet;
import de.sabbertran.proxysuite.ProxySuite;
import de.sabbertran.proxysuite.utils.Warp;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.HashSet;
import java.util.Set;

public class DelWarpCommand extends Command implements TabExecutor {
    private ProxySuite main;
    private DelWarpCommand self;

    public DelWarpCommand(ProxySuite main) {
        super("delwarp");
        this.main = main;
        this.self = this;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 1) {
            return ImmutableSet.of();
        }

        Set<String> matches = new HashSet<>();

        for (Warp warp : main.getWarpHandler().getWarps()) {
            matches.add(warp.getName());
        }

        return matches;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        main.getCommandHandler().executeCommand(sender, "delwarp", () -> {
            if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.delwarp")) {
                if (args.length == 1) {
                    boolean includeHidden = main.getTeleportHandler().canIgnoreCooldown(sender);
                    Warp w = main.getWarpHandler().getWarp(args[0], includeHidden);
                    if (w != null) {
                        main.getWarpHandler().deleteWarp(w);
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("warp.deleted" +
                                ".success").replace("%warp%", w.getName()));
                    } else {
                        main.getMessageHandler().sendMessage(sender, main.getMessageHandler().getMessage("warp" +
                                ".notexists").replace("%warp%", args[0]));
                    }
                } else {
                    main.getCommandHandler().sendUsage(sender, self);
                }
            } else {
                main.getPermissionHandler().sendMissingPermissionInfo(sender);
            }
        });
    }
}
