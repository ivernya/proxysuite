package de.sabbertran.proxysuite.commands;

import de.sabbertran.proxysuite.ProxySuite;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ImportFromMaSuite extends Command {
    private ProxySuite main;

    public ImportFromMaSuite(ProxySuite main) {
        super("importfrommasuite");
        this.main = main;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        main.getCommandHandler().executeCommand(sender, "importfrommasuite", () -> {
            if (main.getPermissionHandler().hasPermission(sender, "proxysuite.commands.importfrommasuite")) {
                main.getProxy().getScheduler().runAsync(main, () -> {
                    try {
                        PreparedStatement pst = main.getSQLConnection().prepareStatement("SELECT * FROM masuite_homes;");
                        ResultSet rs = pst.executeQuery();
                        while (rs.next()) {
                            System.out.println("Found a home");
                            String server = rs.getString("server");
                            String world = rs.getString("world");
                            double x = rs.getDouble("x");
                            double y = rs.getDouble("y");
                            double z = rs.getDouble("z");
                            float yaw = rs.getFloat("yaw");
                            float pitch = rs.getFloat("pitch");
                            String name = rs.getString("name");
                            String ownerUuid = rs.getString("owner");

                            main.getHomeHandler().setHome(ownerUuid, name, server, world, x, y, z, yaw, pitch);
                        }

                        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.YELLOW + "Successfully imported homes from masuites"));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
            } else {
                main.getPermissionHandler().sendMissingPermissionInfo(sender);
            }
        });
    }
}
